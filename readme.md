These are the talking notes (including LaTeX source) for my Café Scientifique Manchester discussion on 29th Jan 2015. They are in the 'tufte-handout' style which you'll need to get to compile them fresh - but the .pdf is here anyhow.

# Café Scientifique Manchester

Cafe Scientifique is a place where, for the price of a cup of coffee or a glass of wine, anyone can come to explore the latest ideas in science and technology. Meetings take place in cafes, bars, restaurants and even theatres, but always outside a traditional academic context.

Cafe Scientifique is a forum for debating science issues, not a shop window for science. We are committed to promoting public engagement with science and to making science accountable.

Where: Kro Bar on Oxford Road

http://www.cafescientifique.manchester.ac.uk/

## How the Extra-ordinary Informs the Ordinary (29 January 2015)
### Speaker Dr Simon Harper

Talking computers are vital if you're a blind computer user. Blind people become expert in auditory cognition and interaction. This expertise has transferred to the digital tools used to access computers, mobiles and other devices. Indeed, when it comes to technology usage blind people have become uber users - expert in digital interaction enabling increasingly meaningful interaction with tools and services in everyday life, work and study.

In doing so, blind users often find ways around badly developed electronic devices and resources, providing insights and challenges for designers and developers.

This talk will give insights into these extra ordinary digital tools and the people who use them, discussing how neurophysiology points the way to enhanced auditory comprehension, and discusses how 'auditory display' is helpful to everyone... how this extra-ordinary interaction informs the ordinary.